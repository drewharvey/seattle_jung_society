<?php
/*
Plugin Name: Example Plugin with Widget
Plugin URI: http://www.drewharvey.com
Description: This is a simple  plugin with a widget
Version: 0.1
Author: Drew Harvey
Author URI: http://www.drewharvey.com
License: GPL2
*/


class Example_Plugin extends WP_Widget {

	//
	// Setup the plugin and initialize the widget
	//
    function Example_Plugin() {
        parent::WP_Widget(false, $name = __('Example Widget', 'Example_Widget') );
    }

    //
	// Setup the form and fields for the widget in 
	// Admin > Appearance > Widgets
	//
	function form($instance) {

		// Check values
		if ($instance) {
		     $title = esc_attr($instance['title']);
		     $text = esc_attr($instance['text']);
		} else {
		     $title = '';
		     $text = '';
		}

		// Show widget fields in admin > appearance > widgets
		?>
		<!-- widget title -->
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">
				<?php _e('Widget Title', 'Example_Widget'); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<!-- widget text field -->
		<p>
			<label for="<?php echo $this->get_field_id('text'); ?>">
				<?php _e('Text:', 'Example_Widget'); ?>
			</label>
			<input class="widefat" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>" type="text" value="<?php echo $text; ?>" />
		</p>
		<?php
	}

	//
	//	Update the widget fields when an admin presses "save" in
	//	Admin > Appearance > Widgets
	//
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['text'] = strip_tags($new_instance['text']);
		return $instance;
	}

	//
	// The way the widget is displayed on the website
	//
	function widget($args, $instance) {

		// get wordpress options
		extract( $args );

		// get field values
		$title = apply_filters('widget_title', $instance['title']);
		$text = $instance['text'];

		echo $before_widget;

		// show title if exists
		if ($title) {
		  echo $before_title . $title . $after_title;
		}

		// show text if exists
		if ($text) {
		  echo '<p>'.$text.'</p>';
		}

		echo $after_widget;
	}
}

// Register widget with worpdress
add_action('widgets_init', create_function('', 'return register_widget("Example_Plugin");'));

?>
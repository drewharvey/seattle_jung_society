<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title(' | ', true, 'right'); ?></title>
    <!-- CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/kickstart.css" media="all" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	<!-- Javascript -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/kickstart.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
    <?php wp_head(); ?>
</head>
<body>
    <!--- Desktop header -->
    <div id="beforeHeader" class="hide-tablet hide-phone clearfix">
        <div class="grid">
            <div class="col_6">
                Phone: (206) 547-3956 <span style="padding: 0 10px;">|</span> 
                <a href="mailto:office@jungseattle.org">Email: office@jungseattle.org</a>
            </div>
            <div class="col_6">
                <div style="text-align:right;">
                    <a href="<?php bloginfo('url'); ?>/donations/">Support Our Work</a> <span style="padding: 0 10px;">|</span>
                    <a href="<?php bloginfo('url'); ?>/contact/">Contact Us</a> <span style="padding: 0 10px;">|</span>
                    <a href="#">Signup for Newsletter</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div id="header" class="hide-tablet hide-phone clearfix">
        <div class="grid">   
            <!-- website logo -->
            <div class="col_1">
                <a href="<?php bloginfo('url'); ?>">
                    <img style="float:left;" src="<?php echo get_template_directory_uri(); ?>/images/seattle-jung-society-logo-sm.png" alt="Seattle Jung Society logo small" /> 
                </a>     
            </div>
            <!-- website title -->
            <div id="websiteTitle" class="col_3">
                <a href="<?php bloginfo('url'); ?>">
                    <?php echo esc_html( get_bloginfo('name') ); ?>
                </a>
            </div>
            <!-- main navigation -->
            <nav class="col_8 vertCenterContainer">
                <ul class="horizNav">
                    <li><a href="<?php echo esc_html( get_bloginfo('url') ); ?>">Home</a></li>
                    <?php wp_nav_menu( array( 'items_wrap' => '%3$s' ) ); ?>
                </ul>  
            </nav>
            <!-- donate button --
            <div class="col_1">
                <a href="?page_id=94"><img src="http://thecarlosgfigueroafoundation.com/wp-content/uploads/2013/04/paypal-donate-button11.png" alt="Paypal donate button" /></a>
            </div>-->
        </div>  
    </div>

    <!-- push content below menu -->
    <div class="hide-desktop" style="height: 40px;">&nbsp;</div>

    <!-- Mobile header -->
    <div id="headerMobile" class="hide-desktop clearfix">
        <!-- website title -->
        <a id="mobileTitle" class="title" href="<?php bloginfo('url'); ?>">
            <?php echo esc_html (get_bloginfo('name')); ?>
        </a>
        <!-- menu -->
        <a class="mobileBarButton toggleMenu" href="javascript:void(0)">Menu <i class="icon-reorder"></i></a>
        <!-- donate -->
        <a class="mobileBarButton" style="background: green;" href="javascript:void(0)">Donate</a>
        <div class="clear">&nbsp;</div>
        <nav id="mainNavMobile" class="mobileMenu hide">
            <ul>
                <li><a href="<?php echo esc_html( get_bloginfo('url') ); ?>">Home</a></li>
                <?php wp_nav_menu( array( 'items_wrap' => '%3$s' ) ); ?>
            </ul>  
        </nav>
        <!-- menu button functionality -->
    </div>
    
    <!-- sub navigation -->
    <?php
    if ($post->post_parent) {
        $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
    } else if ($post->ID) {
        $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
    }
    if ($children) { ?>
        <div id="belowHeader">
            <div class="grid">
                <ul id="subNav" class="altHorizNav">
                    <?php echo $children; ?>
                </ul>
                <div class="clear">&nbps;</div>
            </div>
        </div>
        <div class="clear">&nbsp;</div>
    <?php } ?>

    <!-- /sub navigation -->
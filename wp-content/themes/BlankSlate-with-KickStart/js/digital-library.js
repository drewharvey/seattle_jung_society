/* ********************************************
 * Digital Library functions
 * Author: Drew Harvey
 *********************************************/

var FilterTable = function($table, searchString) {
    if (searchString === undefined || searchString.length < 1)
        return;

    searchString = searchString.toLowerCase();
    var searchKeys = searchString.split(" "); 
    var numRows = $table.find("tbody").find("tr").length;
    var rowContents;
    var numMatches = 0;

    var $row;
    for (var i = 0; i < numRows; i++) {

        // get the current rows html
        $row = $($table.find("tbody").find("tr").get(i));

        // no search key
        if (searchString === "" || searchString === undefined) {
            $row.show();
        }

        // search key entered
        else {

            // check the row with each individual search key
            for (var j = 0; j < searchKeys.length; j++) {
                numMatches += $row.html().toLowerCase().search(searchKeys[j]);
            }               

            // no matches
            if (numMatches < 1) {
                $row.hide();
            }

            // matches found
            else {
                $row.show();
            }
        }

        // reset match counter
        numMatches = 0;
    }
}

var hasAgreedToTerms = function() {
    return getCookie("digital-library-agreement") !== null;
}

var agreedToTerms = function() {
    setCookie("digital-library-agreement", "agreed", "5");
}

var setCookie = function(name, value, daysBeforeExpire) {
    var timestamp = new Date();

    // convert to from days to ms
    daysBeforeExpire *= 24 * 60 * 60 * 1000

    // add our expiration time to the current timestamp
    timestamp.setTime(timestamp.getTime() + daysBeforeExpire);

    // create our new cookie
    document.cookie = name + "=" + value + "; " + "expires=" + timestamp.toGMTString();
}

var getCookie = function(name) {
    name += "=";

    // split cookie string into single cookies
    var ca = document.cookie.split(';');

    // go thru each cookie and look for our name
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }

    // we could not find the cookie
    return null;
}



$(document).ready(function() {

    // selectors
    var $mediaTable = $("#media-list");
    var $searchField = $("input[name=search]");

    // run on page load
    FilterTable($mediaTable, $searchField.val());

    // user agreement
    if (!hasAgreedToTerms()) {
        $("#agreement").show();
    }

    // event: seach filter 
    $searchField.on('input', function() {
        FilterTable($mediaTable, $searchField.val());
    });

    // event: user agreement check
    $("input[name=agreeToTerms]").on("click", function() {
        agreedToTerms();
        $("#agreement").delay(250).slideToggle("slow");
    });

});
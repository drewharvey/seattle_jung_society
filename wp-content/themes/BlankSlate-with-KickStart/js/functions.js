/* ********************************************
 * General site functions and fixes
 * Author: Drew Harvey
 *********************************************/

var fixFooterAtBottom = function($footer) {
    var footerOffset = $footer.offset().top;
    var footerHeight = $footer.height();
    var windowHeight = window.innerHeight;

    if ( (footerOffset + footerHeight) < windowHeight) {
        $footer.height(windowHeight - footerOffset + "px");
    }
}

var matchHeights = function($el1, $el2, useTallerHeight) {
	if ($el1.height() > $el2.height() && useTallerHeight === true) {
		$el2.height($el1.height() + "px");
	}
	else {
		$el1.height($el2.height() + "px");
	}
}

var matchSize = function($container, $inner) {

    // match elements heights if needed
    if ($inner.height() < $container.height()) {
        $inner.height($container.height());
    }

    // match elements widths if needed
    if ($inner.width() < $container.width()) {
        $inner.width($container.width());
    }
}

var verticallyCenter = function($container, $inner) {

    var heightDiff = $container.height() - $inner.height();
    var topSpacing = heightDiff / 2;

    if ($inner.css("position") == "absolute") {
        $inner.css("top", topSpacing + "px");
    }
    else {
        $inner.css("margin-top", topSpacing + "px");
    }
}

var highlightCurrentNav = function($nav) {
    var currentUrl = document.URL;
    var menuItemUrl = "";
    $nav.children("li").each(function() {
        menuItemUrl = $(this).children("a").attr('href');
        if (currentUrl == menuItemUrl) {
            $(this).addClass("current");
        }
    });
}

var toggleMenu = function($menu) {
    // make sure we have menu to toggle
    if ($menu.length < 1)
        return false;
    // only toggle first menu
    $menu = $($menu[0]);
    $menu.slideToggle("slow");
}

var isMobile = function() {
    return window.innerWidth < 1024;
}

// Run after page images and resources are loaded
$(window).load(function() {

	// if there is empty room below the footer, stretch the footer
	fixFooterAtBottom($("#footer"));

	// make sidebar as tall as main content
    $sidebar = $("#sidebar");
    $content = $("#mainContent .popout");
	if (!isMobile() && $content.length == 1) {
		matchHeights($content, $sidebar, true);
	}

    // only vert center title if we are at the top of the page
    if ($("#header").hasClass("sticky") === false) {
        verticallyCenter($("#header"), $("#websiteTitle"));
    }

	// vert center front page slide show pic and text
    verticallyCenter($(".frontpageSlideshow"), $(".overlay"));
    verticallyCenter($(".overlay"), $(".overlay img"));

    // scale the background image to show correctly in diff screens
    matchSize($(".frontpageSlideshow"), $(".background"));
});

$(document).ready(function() {  

	// highlight current page on subnav
	highlightCurrentNav($("#subNav"));

	// toggle mobile menus if "menu" button is pressed
	$(".toggleMenu").click(function() {
	    toggleMenu($(this).siblings(".mobileMenu"));
	});

    // vert center front page events
    $("#events").children().each(function() {
        verticallyCenter($(this), $(this).children().first());
    });

    // sticky navigation vars
    var $nav = $("#header");
    var $preNav = $("#beforeHeader");
    var $title = $("#websiteTitle");
    var $parent = $nav.parent();
    var stickyHeight = $nav.height();
    var originalMargin = $parent.css("margin-top") | 0;
    // sticky navigation on scroll
    $(window).scroll(function(){
        if ($(window).scrollTop() > stickyHeight) {
            if ($nav.hasClass("sticky") == false) {
                $nav.hide();
                $preNav.hide();
                $nav.addClass("sticky");
                $parent.css("margin-top", stickyHeight);
                $nav.slideDown();
            }
        }
        else {
            $nav.removeClass('sticky');
            $parent.css("margin-top", originalMargin);
            $preNav.show();
        }
    });
});
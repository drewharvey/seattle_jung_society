<?php
/*
Template Name: Library Page
*/
?>

<?php get_header(); ?>

<!-- flow player -->
<link rel="stylesheet" href="//releases.flowplayer.org/5.4.6/skin/minimalist.css">
<script src="//releases.flowplayer.org/5.4.6/flowplayer.min.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/js/digital-library.js"></script>


 <!--- MAIN BODY -->
    
    <!-- 2 COLUMN - SIDEBAR ON RIGHT -->
    <div id="pageTitle" class="grid">
        <!-- page title -->
        <div class="col_9">
            <h1>Library</h1>
        </div>
        <div class="col_3" style="margin-top: 20px;">
            <?php get_search_form(); ?>
        </div>
        <!-- /page title -->

    <!-- main content body -->
    <div id="mainContent" class="col_9">
        <div class="popout">

        <?php

            ///////////////////////////////////////////////////////////
            // ID3 Tags Example
            ///////////////////////////////////////////////////////////

            
            // include getID3() library
            require_once('extensions/getid3/getid3.php');
            
            // Initialize getID3 engine
            $getID3 = new getID3;

            // setup args to get all attachments
            $args = array( 
                'post_type' => 'attachment',        // look in wp attachments
                'post_mime_type' => 'audio/mpeg',   // audio only
                'posts_per_page' => -1,             // unlimited
                'post_status' => 'any',             // draft, published, etc
                'post_parent' => null ); 
            
            // grab all attachments and place into array    
            $attachments = get_posts( $args );
        
            
            if ($attachments) {
                foreach ($attachments as $post) {
                    
                    setup_postdata($post);
                    
                    // get the attachment location
                    $fileLocation = get_attached_file($post->ID);
                    $fileUrl = wp_get_attachment_url($post->ID);
                    
                    // Analyze file and store returned data in $ThisFileInfo
                    $fileInfo = $getID3->analyze($fileLocation);
                    
                    /********************
                     * Supported array values
                     * 
                     * ["id3v2"]["comments"]
                        * album
                        * encoded_by
                        * title
                        * year
                        * genre
                        * publisher
                        * band
                        * composer
                        * artist
                        * picture
                    * *********************/

                    // copies id3v1 and id3v2 values into the ["comments"] array
                    // getid3_lib::CopyTagsToComments($fileInfo);
                    var_dump($fileInfo);
                    // store file info into simple vars
                    $artist = $fileInfo["id3v2"]["comments"]["artist"][0];
                    $album = $fileInfo["id3v2"]["comments"]["album"][0];
                    $title = $fileInfo["id3v2"]["comments"]["title"][0];
                    $url = wp_get_attachment_url($post->ID);
                    
                    // add audio file to specific artist/album
                    $audioFiles[$artist][$album][] = array($title, $url);
                    
                }
                wp_reset_postdata();
            }
            
            /***************************************** 
            * Simple output example *****************
            *****************************************/
            
            // Artists
            foreach ($audioFiles as $artistKey => $artistVal) {
                echo "<h3>" . $artistKey . "</h3>";
                
                // Albums
                foreach ($artistVal as $albumKey => $albumVal) {
                    echo "<h4>Album: " . $albumKey . "</h4>";
                    
                    // Tracks
                    foreach ($albumVal as $titleKey => $titleVal) {
                        echo "<p><a href='" . $titleVal[1] . "'>" . $titleVal[0] . "</a></p>";
                    }
                }
            }
            echo "<hr />";
            
            /***************************************** 
            * Sortable table output *****************
            *****************************************
            
            echo "<h2>Sortable Table Example</h2>";
            echo "<table class='striped sortable' cellspacing='0' cellpadding='0'>";
            echo "<thead><tr>";
            echo "<th>Artist</th>";
            echo "<th>Album</th>";
            echo "<th>Track</th>";
            echo "</tr></thead>";
            echo "<tbody>";
            
            foreach ($audioFiles as $artistKey => $artistVal) {
                foreach ($artistVal as $albumKey => $albumVal) {
                    foreach ($albumVal as $titleKey => $titleVal) {
                        echo "<tr>";
                        echo "<td>" . $artistKey . "</td>";
                        echo "<td>" . $albumKey . "</td>";
                        echo "<td><a href='" . $titleVal[1] . "'>" . $titleVal[0] . "</a></td>";
                        echo "</tr>";
                    }
                }
            }
            
            echo "</tbody></table>";
            */
            ?>

            <?php
            ///////////////////////////////////////////////////////////
            // Media Library Settings
            ///////////////////////////////////////////////////////////

            define("MEDIA_FILE_PATH", get_bloginfo('url') . "/wp-content/uploads/media_library/");
            define("IMAGE_FILE_PATH", get_bloginfo('url') . "/wp-content/uploads/media_library/images/");

            ?>

            <?php
            ///////////////////////////////////////////////////////////
            // Run on Page Load
            ///////////////////////////////////////////////////////////

            $isSingleMediaItem = $_GET['media_item'] ? true : false;
            $isAdminArea = $_GET['admin'] ? true : false;
            $searchText = $_GET['search'] ? $_GET['search'] : "";
            $isUserLoggedIn = is_user_logged_in();

            ?>

            <?php 
            ///////////////////////////////////////////////////////////
            // Verify that the user is logged in
            ///////////////////////////////////////////////////////////

            /*if (!$isUserLoggedIn) : ?>*/
            if (false) : ?>
                <div class="notice error">
                    <p>You must login in order to access the online digital library.</p>
                    <p><a href="#" class="button">Login</a></p>
                </div>

            <?php exit(); endif;  ?>


            <!-- Digital library agreement -->
            <div id="agreement" class="notice success" style="display: none;">
                <h2>Digital Library Terms of Use Agreement</h2>
                <p>Please read and agree to the terms below before accessesing the digital library resources.</p>
                <p>This is an example of a popup disclaimer.</p>
                I agree to the terms of use <input type="checkbox" name="agreeToTerms" />
            </div>

            <?php

            ///////////////////////////////////////////////////////////
            // Display item details if one is selected
            ///////////////////////////////////////////////////////////

            if ($isSingleMediaItem) :

                $args['media_item_id'] = $_GET['media_item'];

                // use the first item since it returns an array
                $item = GetMediaItems($args);
                $item = $item[0];

                // for easy reading
                $title = $item->GetTitle();
                $description = $item->GetDescription();
                $length = $item->GetLength();
                $fileSize = $item->GetSize();
                $fileFormat = $item->GetFormat(); 
                $fileLocation = $item->GetFilePath();;
                $mediaType = $item->GetType();
                $category = $item->GetCategories();
                ?>

                <h2 class="hasSub"><?php echo $title; ?></h2>
                <p class="note">
                    Category: <a href="?search=<?php echo $category; ?>"><?php echo $category; ?></a> 
                    | Type: <a href="?search=<?php echo $mediaType; ?>"><?php echo $mediaType; ?></a>  
                    | Length: <?php echo $length; ?> 
                </p>
                <p><?php echo $description; ?></p>
                <?php if ($mediaType == "Video") : ?>
                    <div class="flowplayer">
                        <video>
                            <source type="video/<?php echo $fileFormat; ?>" src="<?php echo $fileLocation ?>">
                        </video>
                    </div>
                <?php endif; ?>

                <?php if ($mediaType == "Audio") : ?>
                    <audio controls>
                        <source type="audio/<?php echo $fileFormat; ?>" src="<?php echo $fileLocation ?>">
                    </audio>
                <?php endif; ?>

                <p class="note">
                    <a class="button blue small" href="<?php echo $fileLocation; ?>">Download</a> 
                    | Size: <?php echo $fileSize; ?> 
                    | Format: <a href="?search=<?php echo $fileFormat; ?>"><?php echo $fileFormat; ?></a> 
                </p>
               
                <hr />

                <div>
                    <h3>About the Speakers</h3>

                    <?php
                    foreach ($item->GetSpeakers() as $speaker) :
                        $name = $speaker->GetFirstName() . " " . $speaker->GetLastName();
                        $firstName = $speaker->GetFirstName();
                        $lastName = $speaker->GetLastName();
                        $bio = $speaker->GetBio();
                        $imagePath = $speaker->GetImagePath();
                    ?>

                        <div class="col_3">
                            <img src="<?php echo $imagePath; ?>" alt="<?php echo $name; ?>" />
                        </div>
                        <div class="col_9">
                            <h4>
                                <a href="?search=<?php echo $lastName . ", " . $firstName; ?>">
                                    <?php echo $firstName . " " . $lastName; ?>
                                </a>
                            </h4>
                            <p><?php echo $bio; ?></p> 
                            <p>
                                <a href="?search=<?php echo $lastName . ", " . $firstName; ?>">
                                    View all entries that include <?php echo $firstName; ?>
                                </a>
                            </p>
                        </div>
                        <div class="clear">&nbsp;</div>

                    <?php endforeach; ?>
                </div>
                

            <?php endif; ?>

            <?php
            ///////////////////////////////////////////////////////////
            // Display administrative side
            ///////////////////////////////////////////////////////////

            if ($isAdminArea) : ?>

            <h2>All Media Items</h2>

            <!-- filter form -->
            <form>
                <input type="text" name="search" placeholder="Search for media by entering keywords" />
            </form>

            <!-- list of media files -->
            <?php ListMediaItems(true); ?>

            <hr />

            <h2>Add New Media Item</h2>
            <form>
                <p>
                    <label for="file">File:</label>
                    <input type="file" name="file" />
                </p>
                <p>
                    <input type="checkbox" name="use_id3" checked="true" />
                    <label for="use_id3">Automatically fill details with ID3 Tags</label>
                </p>
                <div id="newMediaItemDetails">
                    <p>
                        <label for="title">Title:</label>
                        <input type="text" name="title" placeholder="Title of media" />
                    </p>
                    <p>
                        <label for="type">Type of Media:</label><br>
                        <select name="type">
                            <option value="audio">Audio</option>
                            <option value="video">Video</option>
                        </select>
                    </p>
                    <p>Speakers:<br />
                        <?php 
                        $speakers = GetSpeakers();
                        foreach ($speakers as $speaker) : ?>
                            <input type="checkbox" name="<?php echo $speaker['first_name'] . "_" . $speaker['last_name']; ?>" />
                            <label for="<?php echo $speaker['first_name'] . "_" . $speaker['last_name']; ?>">
                                <?php echo $speaker['last_name'] . ", " . $speaker['first_name']; ?>
                            </label><br />
                        <?php endforeach; ?>
                        <a href="javascript:void()"><i class="icon-plus-sign"></i> Add New Speaker</a>
                    </p>
                    <p>Categories:<br />
                        <?php 
                        $categories = GetCategories();
                        foreach ($categories as $category) : ?>
                            <input type="checkbox" name="<?php echo $category['category_name']; ?>" />
                            <label for="<?php echo $category['category_name']; ?>">
                                <?php echo $category['category_name']; ?>
                            </label><br />
                        <?php endforeach; ?>
                        <a href="javascript:void()"><i class="icon-plus-sign"></i> Add New Category</a>
                    </p>
                    <p><input type="submit" value="Submit" /></p>
                </div>
            </form>

            <?php endif; ?>         

            <?php

            ///////////////////////////////////////////////////////////
            // Display default page
            ///////////////////////////////////////////////////////////

            if (!$isAdminArea && !$isSingleMediaItem) :

            // show wordpress content
            if ( have_posts() ) : while ( have_posts() ) : 
                the_post();
                if ( has_post_thumbnail() ) { the_post_thumbnail(); } 
                the_content(); 
            endwhile; endif;
            ?>

            <h2>Audio and Video Resources</h2>

            <!-- filter form -->
            <form>
                <input type="text" name="search" placeholder="Search for media by entering keywords here" value="<?php echo $searchText; ?>" />
            </form>

            <?php ListMediaItems(false); ?>

            <?php endif; ?>
        </div>
    </div>
    <!-- /main content body -->

<?php get_sidebar(); ?>

</div>
<div class="clear"></div>
	
<?php get_footer(); ?>


<?php

function GetFileLocation($filename) {
    return MEDIA_FILE_PATH . $filename;
}

function GetImageLocation($filename) {
    return IMAGE_FILE_PATH . $filename;
}

function FormatPlaybackLength($seconds) {
    $length = ($seconds / 60);
    $length = number_format($length, '2');
    return $length . " minutes";
}

function FormatFileSize($kb) {
    $size = ($kb / 1000);
    $size = number_format($size, '2');
    return $size . "MB";
}

function GetCategories() {
    $categories = array();
    $query = mysql_query("SELECT * FROM wp_media_lib_category ORDER BY category_name");
    while ($category = mysql_fetch_assoc($query)) {
        $categories[] = $category;
    }
    return $categories;
}

function GetSpeakers() {
    $speakers = array();
    $query = mysql_query("SELECT * FROM wp_media_lib_speaker ORDER BY last_name");
    while ($speaker = mysql_fetch_assoc($query)) {
        $speakers[] = $speaker;
    }
    return $speakers;
}

function GetMediaItems($args) {

    $mediaItems = array();

    // setup standard query
    $queryStr = "
        SELECT *

        FROM `wp_media_lib_media_item`

        /* get speakers */
        LEFT JOIN `wp_media_lib_item_speaker`
        ON `wp_media_lib_media_item`.media_item_id = `wp_media_lib_item_speaker`.media_item_id
        LEFT JOIN `wp_media_lib_speaker`
        ON `wp_media_lib_item_speaker`.speaker_id = `wp_media_lib_speaker`.speaker_id

        /* get categories */
        LEFT JOIN `wp_media_lib_item_category`
        ON `wp_media_lib_media_item`.media_item_id = `wp_media_lib_item_category`.media_item_id
        LEFT JOIN `wp_media_lib_category`
        ON `wp_media_lib_item_category`.category_id = `wp_media_lib_category`.category_id
        
        /* get media type */
        LEFT JOIN `wp_media_lib_media_type`
        ON `wp_media_lib_media_item`.media_type_id = `wp_media_lib_media_type`.media_type_id 

        /* start WERE clause so we can add to it easily */
        WHERE 1=1 
    ";

    // apply any arguments passed in
    if (isset($args["media_item_id"])) {
        $queryStr .= " AND `wp_media_lib_media_item`.media_item_id='" . $args["media_item_id"] . "' ";
    }

    // apply order by
    $queryStr .= "ORDER BY `title`";

    // query and return results
    $query = mysql_query($queryStr);

    $itemMap = array();
    $speakerMap = array();

    while ($itemData = mysql_fetch_assoc($query)) {

        $itemID = $itemData['media_item_id'];
        $speakerID = $itemData['speaker_id'];

        // use existing object if already created
        if (isset($itemMap[$itemID])) {
            $item = $itemMap[$itemID];
        }
        // create new obj
        else {
            
            $item = new MediaItem();
            $item->SetData($itemData);
            $itemMap[$itemID] = $item;
            $mediaItems[] = $item;
        }

        // use existing obj if already created
        if (isset($speakerMap[$speakerID])) {
            $speaker = $speakerMap[$speakerID];
        }
        // create new obj
        else {
            $speaker = new Speaker();
            $speaker->SetData($itemData);
            $speakerMap[$speakerID] = $speaker;
        }
        $item->AddSpeaker($speaker); 
    }

    return $mediaItems;
}


function ListMediaItems($isAdmin) {

    $mediaItems = GetMediaItems(null);

    echo "<table id='media-list' class='striped sortable' cellspacing='0' cellpadding='0'>";
    echo "<thead><tr>";
    echo "<th>Title</th>";
    echo "<th>Speaker</th>";
    echo "<th>Category</th>";
    echo "<th>Type</th>";

    if ($isAdmin) {
        echo "<th></th>";
        echo "<th></th>";
    }
    
    echo "</tr></thead>";
    echo "<tbody>";
    
    foreach ($mediaItems as $item) {

        $url = "?media_item=" . $item->GetID();
        $speakers = "";
        foreach ($item->GetSpeakers() as $speaker ) {
            $speakers .= $speaker->GetLastName() . ", " . $speaker->GetFirstName() . "<br />";
        }

        echo "<tr>";
        echo "<td><a href='$url'>" . $item->GetTitle(). "</a></td>";
        echo "<td>" . $speakers . "</td>";
        echo "<td>" . $item->GetCategories() . "</td>";
        echo "<td>" . $item->GetType() . "</td>";

        if ($isAdmin) {
            echo "<td><a href='javascript:void()''><i class='icon-edit'></i></a></td>";
            echo "<td><a href='javascript:void()''><i class='icon-minus-sign'></i></a></td>";
        }
        
        echo "</tr>";
    }
    
    echo "</tbody></table>";
}

?>

<?php

class MediaItem {

    private $id;
    private $title;
    private $description;
    private $mediaType;
    private $length;
    private $fileFormat;
    private $fileSize;
    private $filePath;
    private $category;

    private $speakerList;
    private $speakerMap;

    function __construct() {
        $speakerList = array();
    }

   function SetData($data) {
        $this->id = $data['media_item_id'];
        $this->title = $data['title'];
        $this->description = $data['description'];
        $this->length = FormatPlaybackLength($data['length']);
        $this->fileSize = FormatFileSize($data['file_size']);
        $this->fileFormat = $data['file_format']; 
        $this->filePath = GetFileLocation($data['file_name']);
        $this->mediaType = $data['media_type_name'];
        $this->category = $data['category_name'];
    }

    function AddSpeaker($speaker) {
        // check if we already have this speaker
        if (isset($this->speakerMap[$speaker->GetID()])) {
            return;
        }

        $this->speakerList[] = $speaker;
        $this->speakerMap[$speaker->GetID()] = $speaker;
    }
    
    function GetID() {
        return $this->id;
    }

    function GetTitle() {
        return $this->title;
    }

    function GetDescription() {
        return $this->description;
    }

    function GetType() {
        return $this->mediaType;
    }

    function GetLength() {
        return $this->length;
    }

    function GetFormat() {
        return $this->fileFormat;
    }

    function GetSize() {
        return $this->fileSize;
    }

    function GetFilePath() {
        return $this->filePath;
    }

    function GetCategories() {
        return $this->category;
    }

    function GetSpeakers() {
        return $this->speakerList;
    }
}

class Speaker {

    private $id;
    private $firstName;
    private $lastName;
    private $bio;
    private $imagePath;

    function __construct() {

    }

    function SetData($data) {
        $this->id = $data['speaker_id'];
        $this->firstName = $data['first_name'];
        $this->lastName = $data['last_name'];
        $this->bio = $data['speaker_bio'];
        $this->imagePath = GetImageLocation($data['speaker_image_file_name']);
    }

    function GetID() {
        return $this->id;
    }

    function GetFirstName() {
        return $this->firstName;
    }

    function GetLastName() {
        return $this->lastName;
    }

    function GetBio() {
        return $this->bio;
    }

    function GetImagePath() {
        return $this->imagePath;
    }
}

?>

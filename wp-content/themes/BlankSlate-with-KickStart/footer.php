</div>
<!--- FOOTER -->
    <div id="footer">
        <div class="grid">
            <div class="col_3">
                <p>
                    &copy; <a href="https://maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;i=UTF8&amp;q=C+G+Jung+Society+Seattle&amp;fb=1&amp;gl=us&amp;hq=seattle+jung+society&amp;cid=0,0,6802706866782179319&amp;t=m&amp;ll=47.663989,-122.330749&amp;spn=0.006295,0.006295&amp;source=embed" target="_blank">C G Jung Society, Seattle</a><br />
                    4649 Sunnyside Ave N, Room 222<br />
                    Seattle, WA 98103
                </p>
                <p>
                    Site by <a href="http://drewharvey.com">Drew Harvey</a>
                </p>
            </div>
            <div class="col_3">
                <h3>Contact</h3>
                <p>
                    Phone: (206) 547-3956<br />
                    Fax: (206) 547-5959<br />
                    Email: <a href="mailto:office@jungseattle.org">office@jungseattle.org</a>
                </p>
            </div>
            <div class="col_3">
                <h3>Hours</h3>
                <p>  
                    Monday - Friday: Hours vary, call to confirm<br />
                    Saturday: 12:30pm - 3:30pm<br />
                    Sunday: Building closed
                </p>
            </div>
            <div class="col_3">
                <h3>Connect</h3>
                <p class="icon-2x">
                    <a href="https://www.facebook.com/groups/seattlejungsociety/"><i class="icon-facebook" style="margin-right: 10px;"></i></a>
                    <a href="https://www.twitter.com" ><i class="icon-twitter" style="margin-right: 10px;"></i></a>
                    <a href="http://www.linkedin.com/groups/CG-Jung-Society-Seattle-1111617"><i class="icon-linkedin" style="margin-right: 10px;"></i></a>
                    <a href="<?php bloginfo('url'); ?>/contact/"><i class="icon-envelope-alt"></i></a>
                    <br />&nbsp;
                </p>
                <p>
                    <a href="<?php bloginfo('url'); ?>/wp-login.php"><i class="icon-lock"></i>&nbsp;&nbsp;&nbsp;&nbsp;Member Login</a>
                </p>
            </div>
        </div>
        <div class="clear"></div>
    </div>
<?php wp_footer(); ?>
</body>
</html>
<?php get_header(); ?>

 <!-- MAIN BODY  -->

    <!-- 2 COLUMN - SIDEBAR ON RIGHT -->
    <div id="pageTitle" class="grid">

        <!-- page title -->
        <div class="col_9">
            <h1><?php printf( __( 'Search Results for: %s', 'blankslate' ), get_search_query() ); ?></h1>
        </div>
        <div class="col_3">
            <?php get_search_form(); ?>
        </div>

    <!-- main content body -->
    <div id="mainContent" class="col_9">
	    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	        <div class="popout">
				<?php get_template_part('entry'); ?>		
	        </div>
        <?php endwhile; ?>
	    <?php else : ?>
			<div class="popout">
				<p>Sorry, there were not matching search results.</p>
	        </div>
	    <?php endif; ?>
    </div>
    
    <!-- /main content body -->


<?php get_sidebar(); ?>

</div>
<div class="clear"></div>

<?php get_footer(); ?>
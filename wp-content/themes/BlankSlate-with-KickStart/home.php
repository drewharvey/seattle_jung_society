<?php get_header(); ?>

    <!---FRONT PAGE SLIDESHOW -->
    <div class="frontpageSlideshow hide-phone">
        <img src="<?php echo get_bloginfo('template_url'); ?>/images/seattle-bg-color-flipped.jpg" class="background" alt="Image of Seattle, WA"/>
        <div class="overlay">
            <div class="grid">
                <div class="col_3">
                    <img id="slideshowImage" src="<?php echo get_bloginfo('template_url'); ?>/images/carl-jung-square.jpg" style="width: 100%;" />
                </div>
                <div class="col_9">
                    <h1 class="title">Explore the Seattle Jung Society</h1>
                    <p class="desc">
                        The Society is a nonprofit educational corporation formed to provide a forum for the ideas of C.G. Jung. The Jung Society sponsors lectures, workshops, seminars, and study groups by both locally and nationally known Jungian scholars, and its events are, for the most part, intended for the general public.
                    </p>
                    <p><a class="button redDark" href="<?php echo get_permalink(33); ?>">Learn More</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <!-- Mobile Only -->
    <div class="grid hide-desktop hide-tablet">
        <div class="col_12">
            <h1>Explore the Seattle Jung Society</h1>
            <p>
                The Society is a nonprofit educational corporation formed to provide a forum for the ideas of C.G. Jung. The Jung Society sponsors lectures, workshops, seminars, and study groups by both locally and nationally known Jungian scholars, and its events are, for the most part, intended for the general public.
            </p>
        </div>
        <hr />
    </div>
    
    <!--- MAIN BODY -->
    <div class="grid">
        <div id="events" class="col_12">
            <h2>Upcoming Events</h2>
            <div class="popout noPadding">
                <div class="col_2 date">
                    June 4, 2014
                </div>
                <div class="col_8">
                    <h3 class="hasSub">New Member Orientation</h3>
                    <p class="note">Events | June 4, 2014 | 12:00 am – 12:30 am</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="popout noPadding">
                <div class="col_2 date">
                    June 10, 2014
                </div>
                <div class="col_8">
                    <h3 class="hasSub">Workshop: Art of Focus</h3>
                    <p class="note">Events | June 10, 2014 | 12:00 am – 12:30 am</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
            <div class="popout noPadding">
                <div class="col_2 date">
                    June 25, 2014
                </div>
                <div class="col_8">
                    <h3 class="hasSub">Society Social - Dinner</h3>
                    <p class="note">Events | June 25, 2014 | 12:00 am – 12:30 am</p>
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                    </p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

    <div class="grid center">
        <div class="col_4 popout">
            <h2 class="large"><i class="icon-calendar"></i> Society Events</h2>
            <p>Upcoming seminars and events associated with the Seattle Jung Society.</p>
            <p><a class="button green small" href="<?php bloginfo('url'); ?>/events/">View Events</a></p>
        </div>
        <div class="col_4 popout">
            <h2 class="large"><i class="icon-book"></i> Online Library</h2>
            <p>Online library that contains audio and video lectures, workshops, and seminars.</p>
            <p><a class="button green small" href="<?php bloginfo('url'); ?>/library/">View Library</a></p>
        </div>
        <div class="col_4 popout">
            <h2 class="large"><i class="icon-envelope"></i> Contact Us</h2>
            <p>Get in touch with members of the Seattle Jung Society by email or phone.</p>
            <p><a class="button green small" href="<?php bloginfo('url'); ?>/contact/">Contact Us</a></p>
        </div>
        <div class="clear">&nbsp;</div>
    </div>

<!--
    <div class="popout">
        <div class="grid">
            <div class="col_9">
                <h2>Become a Jung Society Member</h2>
                <p><img class="caption" title="Group of Seattle Jung Society Members in 2013" src="<?php bloginfo('template_url'); ?>/images/group-placeholder.jpg" alt=""></p>
            </div>
            <div class="col_3">
                <h2>Benefits</h2>
                <h3 class="large"><i class="icon-bookmark"></i> Use of the Library</h3>
                <p>Our expanded library includes research hold rare volumes of Jungian thought.</p>

                <h3 class="large"><i class="icon-comment"></i> Admission to Lectures</h3>
                <p>Attend exclusive Seattle Jung Society lectures and workshops.</p>

                <h3 class="large"><i class="icon-user"></i> Admission to Member Events</h3>
                <p>Attend members-only and other special events.</p>

                <p><a href="<?php bloginfo('url'); ?>/membership/" class="button blue">More About Memberships</a></p>
            </div>  
            <div class="clear">&nbsp;</div>
        </div>
    </div>
-->

    <div class="clear"></div>
	
<?php get_footer(); ?>
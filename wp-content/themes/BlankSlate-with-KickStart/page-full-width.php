<?php
/*
Template Name: Full Width
*/
?>

<?php get_header(); ?>

 <!-- MAIN BODY  -->
    <!-- 2 COLUMN - SIDEBAR ON RIGHT -->
    <div id="pageTitle" class="grid">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <!-- page title -->
        <div class="col_9">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="col_3">
            <?php get_search_form(); ?>
        </div>

    <!-- main content body -->
    <div id="mainContent" class="col_12">
        <div class="popout">
            <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
            <?php the_content(); ?>
        </div>
    </div>
    <?php endwhile; endif; ?>
    <!-- /main content body -->

</div>
<div class="clear"></div>

<?php get_footer(); ?>
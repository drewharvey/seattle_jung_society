<!-- Single Event Page (Default single event format) -->

<p>#_CATEGORYNAME | #_EVENTDATES | #_EVENTTIMES</p>
<p>#_EVENTIMAGE</p>
<p>#_EVENTNOTES</p>

{has_location}
	<h2>Event Location</h2>
	<p>
		#_LOCATIONNAME<br />
		#_LOCATIONADDRESS<br />
		#_LOCATIONTOWN, #_LOCATIONSTATE #_LOCATIONPOSTCODE
	</p>
	<p>
		<a href="https://maps.google.com/?q=#_LOCATIONADDRESS,#_LOCATIONTOWN,#_LOCATIONSTATE,#_LOCATIONPOSTCODE">
			Link to Google Map
		</a>
		#_LOCATIONMAP</p>
{/has_location}

{has_bookings}
	<h3>Bookings</h3>
	#_BOOKINGFORM
{/has_bookings}


<!-- Events Page (Default event list format) -->

<h4>#_EVENTLINK</h4>
<p>
	#_CATEGORYNAME | #_EVENTDATES | #_EVENTTIMES
	{has_location}
		<br />
		#_LOCATIONTOWN, #_LOCATIONSTATE
	{/has_location}
</p>

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores, dolorum, atque, ut, autem sunt mollitia neque similique eos adipisci consequatur minima expedita culpa amet! Dolores, eos suscipit molestias et possimus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde, molestiae, earum, dolor repellendus ad ullam ipsam a tempore quibusdam eaque qui dolores sint numquam voluptatum praesentium veritatis quisquam quia corporis.
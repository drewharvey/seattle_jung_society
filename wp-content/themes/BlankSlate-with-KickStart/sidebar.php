<!-- sidebar -->
<div id="sidebar" class="col_3">
    <div class="sidebarContent">

        <!-- sidebar widgets -->
        <h3><a href="<?php echo get_page_link(13); ?>">Upcoming Events</a></h3>
        <p><a href="<?php echo get_page_link(13); ?>">View All Events</a></p>
        <?php echo em_get_events(); ?> 
        <hr />

        <?php // show sidebar widgets
        if (is_active_sidebar('primary-widget-area')) :
            $args = array(
                'id' => 'primary-widget-area',
                'before_widget' => '',
                'after_widget' => '<hr />',
                'before_title' => '<h3>',
                'after_title' => '</h3>'
                );
            register_sidebar($args);
            dynamic_sidebar($args['id']);  
        endif; 
        ?>
        <!-- /sidebar widgets -->

    </div>
</div>
<!-- /sidebar -->
<form class="searchbar" action="<?php echo home_url( '/' ); ?>" method="get">
    <input type="text" class="search" name="s" id="search" placeholder="Search" value="<?php the_search_query(); ?>" />
    <input type="image" class="submit" alt="Submit" src="http://www.talgroup.net/static/graphics/search-icon_white_64.png" />
</form>
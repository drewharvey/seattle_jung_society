<?php get_header(); ?>

    <!---FRONT PAGE SLIDESHOW -->
    <div class="frontpageSlideshow">
        <img src="<?php echo get_bloginfo('template_url'); ?>/images/seattle-bg-color-flipped.jpg" class="background" alt="Image of Seattle, WA"/>
        <div class="overlay">
            <div class="grid">
                <div class="col_3">
                    <img src="<?php echo get_bloginfo('template_url'); ?>/images/carl-jung-square.jpg" />
                </div>
                <div class="col_9">
                    <h1 class="title">Explore the Seattle Jung Society</h1>
                    <p class="desc">
                        The Society is a nonprofit educational corporation formed to provide a forum for the ideas of C.G. Jung. The Jung Society sponsors lectures, workshops, seminars, and study groups by both locally and nationally known Jungian scholars, and its events are, for the most part, intended for the general public.
                    </p>
                    <p><a class="button orange" href="<?php echo get_permalink(33); ?>">Learn More</a></p>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    
    <!--- MAIN BODY -->

    <div class="grid">

        <!-- main content body -->
        <div class="col_9">
            
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="popout">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <p class="note">
                        Posted by Drew Harvey on October 24, 2013
                    </p>
                    <p>
                        <?php the_excerpt(); ?>
                    </p>
                    <p>
                        <a class="button small blue" href="<?php the_permalink(); ?>">Continue Reading</a>
                    </p>
                </div>
            <?php endwhile; endif; ?>
            
        </div>
        
        <!-- /main content body -->
    
        <!-- sidebar -->
        <?php get_sidebar(); ?>
        <!-- /sidebar -->

</div>
<div class="clear"></div>
	
<?php get_footer(); ?>
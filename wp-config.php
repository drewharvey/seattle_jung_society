<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'jung_society');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.ylklM#|De7zE+|]F){~5~Ler&&-7d]!z>x7#NwbG?DYK+`FUDr;D(SeIE!nV]|5');
define('SECURE_AUTH_KEY',  '|SSQoT.5TZl}ehXe`XiS}wA{rKJ/9(I?Yij+&/XnX/FHit0qi;Zl54-gFgLd@l!u');
define('LOGGED_IN_KEY',    'q4%>J`v:I&.2rU_2g4Lh_V;|ye||{n:Z1(AsE]Jdd|UcL*p`1f #KR7M!Zw=.7NC');
define('NONCE_KEY',        'Pz=]e.Iq<j9_yiltYRLH:tEr!QwYa:5gl72AA@Sgs;Ea;<1toTK`}o?G#E:qpfT}');
define('AUTH_SALT',        '%Bcf9;N[lnY$F*=P5+?TU|w9g;[rbE;<Pa+*zzmU$)Fo6G539*ZY`{fCbtil-*J.');
define('SECURE_AUTH_SALT', '-Eg:8}BQG(7,6ug7LS9pxM(sZAjK1tl4[s3X_6L}*]C9z^.T+E9Xq8l9c[+8ZY0>');
define('LOGGED_IN_SALT',   'Ve+2?(JZ:YtI9s+QG@T*+JcDSQfdlL&Y(K+GlET<*Xli9,b6<yOX?fe`OO<C_4pA');
define('NONCE_SALT',       'f^<%b=iv3Q/qQ#Q[*|43Y^DdnQ@|d8 li&>,>(S3W+PLkv+$P/&B06dqoTXff9A8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please! 
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
